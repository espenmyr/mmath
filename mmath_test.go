package mmath

import (
	"fmt"
	"testing"
)

func TestGaussj(t *testing.T) {

	var A = [][]float64{
		{1, 2}, /*  initializers for row indexed by 0 */
		{6, 5}, /*  initializers for row indexed by 1 */
	}

	fmt.Printf("A:\n")
	PrintMatrix(A)

	fmt.Printf("b:\n")
	var b = []float64{3, 4}
	PrintArr(b)

	x, err := Gaussj(A, b)
	if err != nil {
		t.Errorf("Wanted err=nil, got err=%v", err)
	}

	var xtrue = []float64{-1.0, 2.0}
	if VectorEquals(xtrue, x, 2) == false {
		t.Errorf("Want true, got %t", false)
	}
	fmt.Printf("x (Solution) = \n")
	PrintArr(x)

}

func TestGaussj2(t *testing.T) {

	var A = [][]float64{
		{1, 1, 2},  /*  initializers for row indexed by 0 */
		{2, 4, -3}, /*  initializers for row indexed by 1 */
		{3, 6, -5}, /*  initializers for row indexed by 2 */

	}

	fmt.Printf("A:\n")
	PrintMatrix(A)

	fmt.Printf("b:\n")
	var b = []float64{9, 1, 0}
	PrintArr(b)

	x, err := Gaussj(A, b)
	if err != nil {
		t.Errorf("Wanted err=nil, got err=%v", err)
	}

	var xtrue = []float64{1.0, 2.0, 3.0}
	if VectorEquals(xtrue, x, 2) == false {
		t.Errorf("Want true, got %t", false)
	}
	fmt.Printf("x (Solution) = \n")
	PrintArr(x)

}

func TestGaussjSingularMatrix(t *testing.T) {

	var A = [][]float64{
		{12, 10}, /*  initializers for row indexed by 0 */
		{6, 5},   /*  initializers for row indexed by 1 */
	}

	var b = []float64{3, 4}
	_, err := Gaussj(A, b)
	if err != ErrSingular {
		t.Errorf("Wanted error ErrSinglurar:  Singular Matrix Errror, got err=%v", err)
	}

}
