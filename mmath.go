package mmath

import (
	"errors"
	"fmt"
	"math"
)

// ErrSingular - Singlurar matrix - no solutions
var ErrSingular = errors.New("We have singular matrix")

// PrintArr - Print Array / Vector
func PrintArr(arr []float64) {
	for i := 0; i < len(arr); i++ {
		fmt.Printf("%3.0f ", arr[i])
	}
	fmt.Printf("\n")
}

// PrintMatrix - Display 2D matrix
func PrintMatrix(matrix [][]float64) {
	for r := 0; r < len(matrix); r++ {
		for c := 0; c < len(matrix[r]); c++ {
			fmt.Printf("%3.0f ", matrix[r][c])
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")
}

// AugmentedMatrix - In this case, Add a column to matrix
func AugmentedMatrix(a [][]float64, b []float64) [][]float64 {

	M := len(a)
	N := len(a[0])
	var ac = make([][]float64, M)
	for r := 0; r < M; r++ {
		// Make a copy instead of modifying the orig
		//a[r] = append(a[r], b[r])
		ac[r] = make([]float64, N+1)
		copy(ac[r], append(a[r], b[r]))
	}

	return ac
}

// EPSILON - The infamous Epsilon
var EPSILON = 1e-8

// Gaussj - Gauss Jordan elimination
// returns the x-vector in Ax=b
func Gaussj(A [][]float64, b []float64) ([]float64, error) {

	a := AugmentedMatrix(A, b)
	// Number of rows:
	M := len(a)
	// Number of Columns:
	N := len(a[0])

	if M+1 != N {
		return []float64{}, fmt.Errorf("Gaussj; Invalid matrix. Number of rows must be equal in A and b M:%d, N: %d", M, N)
	}

	for c := 0; c < M; c++ {
		max := c
		for r := c + 1; r < M; r++ {
			if math.Abs(a[r][c]) > math.Abs(a[max][c]) {
				max = r
			}
		}
		a = SwapRows(a, c, max)

		// singular
		if math.Abs(a[c][c]) <= EPSILON {
			//fmt.Printf("We have Singular matrix\n")
			return []float64{}, ErrSingular
		}
		a = pivot(a, c, c)
	}

	var x = []float64{}
	for r := 0; r < M; r++ {
		x = append(x, a[r][N-1])
	}
	return x, nil
}
func pivot(a [][]float64, p int, q int) [][]float64 {
	// Number of rows:
	M := len(a)
	// Number of Columns:
	N := len(a[0])

	for i := 0; i < M; i++ {
		f := a[i][q] / a[p][q]
		for j := 0; j < N; j++ {
			if i != p && j != q {
				a[i][j] -= f * a[p][j]
			}
		}
	}

	// Zero out columbn q
	for i := 0; i < M; i++ {
		if i != p {
			a[i][q] = 0.0
		}
	}

	// Normalize  row p
	for j := 0; j < N; j++ {
		if j != q {
			a[p][j] /= a[p][q]
		}
	}
	a[p][q] = 1.0

	return a
}

// SwapRows swap row r with row imax
func SwapRows(a [][]float64, r int, imax int) [][]float64 {
	tmp := a[r]
	a[r] = a[imax]
	a[imax] = tmp
	return a
}

// Equals - Floating point "equals"
func Equals(x float64, y float64) bool {
	return math.Abs(x-y) <= EPSILON
}

// VectorEquals - Floating point "equals"
func VectorEquals(x []float64, y []float64, len int64) bool {
	var i int64
	for i = 0; i < len; i++ {
		if math.Abs(x[i]-y[i]) > EPSILON {
			return false
		}
	}
	return true
}

// Matrix2DEquals - Floating point "equals"
func Matrix2DEquals(x [][]float64, y [][]float64) bool {

	//return math.Abs(x-y) <= EPSILON
	return false
}
